<?php


namespace PhpOO;

class Voiture
{

    public const KLAXON = "Pouet pouet";
    public $couleur = "Bleu";

    public $marque;

    public function Klaxonner()
    {
        echo self::KLAXON;
    }

    public function SeDecrire()
    {
        echo "Je suis de couleur " . $this->couleur;
    }
}