<?php

namespace PhpOO\Models;

class Product
{

    private $productID;
    private $productName;
    private $productDescription;
    private $productImage;

    /**
     * @return int
     */
    public function getProductID()
    {
        return $this->productID;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productID = $productId;
    }

    /**
     * @return int
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param int $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return int
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * @param mixed $productDescription
     */
    public function setProductDescription($productDescription)
    {
        $this->productDescription = $productDescription;
    }

    /**
     * @return mixed
     */
    public function getProductImage()
    {
        return $this->productImage;
    }

    /**
     * @param mixed $productImage
     */
    public function setProductImage($productImage)
    {
        $this->productImage = $productImage;
    }
}