<?php

namespace PhpOO\Models;

class Supplier
{

    private $supplierID;
    private $supplierName;
    private $supplierAdress;
    private $supplierMail;

    /**
     * @return mixed
     */
    public function getSupplierID()
    {
        return $this->supplierID;
    }

    /**
     * @param mixed $supplierID
     */
    public function setSupplierID($supplierID)
    {
        $this->supplierID = $supplierID;
    }

    /**
     * @return mixed
     */
    public function getSupplierName()
    {
        return $this->supplierName;
    }

    /**
     * @param mixed $supplierName
     */
    public function setSupplierName($supplierName)
    {
        $this->supplierName = $supplierName;
    }

    /**
     * @return mixed
     */
    public function getSupplierAdress()
    {
        return $this->supplierAdress;
    }

    /**
     * @param mixed $supplierAdress
     */
    public function setSupplierAdress($supplierAdress)
    {
        $this->supplierAdress = $supplierAdress;
    }

    /**
     * @return mixed
     */
    public function getSupplierMail()
    {
        return $this->supplierMail;
    }

    /**
     * @param mixed $supplierMail
     */
    public function setSupplierMail($supplierMail)
    {
        $this->supplierMail = $supplierMail;
    }
}