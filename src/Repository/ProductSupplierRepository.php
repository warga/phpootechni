<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 20-09-18
 * Time: 11:52
 */

namespace PhpOO\Repository;

use PhpOO\Models\ProductSupplier;
use ToolBox\BaseRepository;

class ProductSupplierRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'productsupplier';
    }

    protected function getPKName()
    {
        return [
            'productID' => 'productID',
            'supplierID' => 'supplierID'
        ];
    }

    protected function getEntityName()
    {
        return ProductSupplier::class;
    }

    protected function getBindings()
    {
        return [
            "price" => "getPrice",
            "productID" => "getProductID",
            "supplierID" => "getSupplierID"
        ];
    }
}