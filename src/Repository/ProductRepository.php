<?php

namespace PhpOO\Repository;

use PhpOO\Models\Product;
use ToolBox\BaseRepository;

class ProductRepository extends BaseRepository
{
    protected function getTableName()
    {
        return 'product';
    }

    protected function getPKName()
    {
        //nom colonne => methode get var
        return ['productID' => 'productID'];
    }

    protected function getEntityName()
    {
        return Product::class;
    }

    protected function getBindings()
    {
        //nom colonne => methode get var
        return [
            "productName" => "getProductName",
            "productDescription" => "getProductDescription",
            "productImage" => "getProductImage"
        ];
    }
}