<?php

namespace PhpOO\Repository;

use PhpOO\Models\Supplier;
use ToolBox\BaseRepository;

class SupplierRepository extends BaseRepository
{
    protected function getTableName()
    {
        return 'supplier';
    }

    protected function getPKName()
    {
        return ['supplierID' => 'supplierID'];
    }

    protected function getEntityName()
    {
        return Supplier::class;
    }

    protected function getBindings()
    {
        return [
            "supplierName" => "getSupplierName",
            "supplierMail" => "getSupplierMail",
            "supplierAdress" => "getSupplierAdress"
        ];
    }
}