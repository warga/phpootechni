<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 18-09-18
 * Time: 13:29
 */

namespace PhpOO\controllers;

use PhpOO\Models\Product;
use PhpOO\Repository\ProductRepository;


class ProductController
{

    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new ProductRepository();
        $prods = $repo->getAll();
        return $this->twig->render('productList.html.twig', [
            "products" => $prods
        ]);
    }

    public function details($id)
    {
        if (is_numeric($id)) {
            $prod = new Product();
            $prod->setProductId($id);
            $repo = new ProductRepository();
            $prods = $repo->getByID($prod);
            echo $this->twig->render('productDetails.html.twig', [
                "product" => $prods
            ]);
        } else {
            header("Location: app.php");
        }
    }

    public function newProd($post = null)
    {

        if(isset($post['name'])){
            $prod = new Product();
            $prod->setProductName($post['name']);
            $prod->setProductDescription($post['description']);
            $prod->setProductImage($post['image']);
            $isValid = true;
            $errors = [];

            if (strlen(trim($post['name'])) === 0) {
                $isValid = false;
                $errors['name'] = "Le champ est requis";
            }
            if (strlen(trim($post['description'])) === 0) {
                $isValid = false;
                $errors['description'] = "Le champ est requis";
            }
            if (strlen(trim($post['image'])) === 0) {
                $isValid = false;
                $errors['image'] = "Le champ est requis";
            }
            if ($isValid) {
                $repo = new ProductRepository();
                if($repo->insert($prod)) {
                    header('location: localhost/phpoo/web/app.php?controller=product&method=index');
                } else {
                    $errors['sql'] = "REQUEST SQL DON'T PASSED !!! :)";

                    return $this->twig->render('productNew.html.twig' , [
                        'prod' => $prod,
                        'errors' => $errors
                    ]);
                }
            } else {
                return $this->twig->render('productNew.html.twig' , [
                    'prod' => $prod,
                    'errors' => $errors
                ]);
            }
        } else {
            return $this->twig->render('productNew.html.twig');
        }
    }
}