<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 18-09-18
 * Time: 13:29
 */

namespace PhpOO\controllers;
use PhpOO\Models\Supplier;
use PhpOO\Repository\SupplierRepository;


class SupplierController
{

    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new SupplierRepository();
        $sups = $repo->getAll();

        echo $this->twig->render('supplierList.html.twig', [
            "suppliers" => $sups
        ]);
    }

    public function details($id)
    {
        if (is_numeric($id)) {
            $sup = new Supplier();
            $sup->setSupplierID($id);
            $repo = new SupplierRepository();
            $sup = $repo->getByID($sup);
            echo $this->twig->render('supplierDetails.html.twig', [
                "supplier" => $sup
            ]);
        } else {
            header("Location: app.php");
        }
    }

    public function newSup($post = null)
    {
        if(isset($post['name'])){
            $isValid = true;

            if ($isValid) {
                $sup = new Supplier();
                $sup->setSupplierName($post['name']);
                $sup->setSupplierAdress($post['adress']);
                $sup->setSupplierMail($post['mail']);
                $repo = new SupplierRepository();
                if($repo->insert($sup)) {
                    header('location: localhost/phpoo/web/app.php?controller=supplier&method=index');
                }
            } else {
                $errors['sql'] = "REQUEST SQL DON'T PASSED !!! :)";

                return $this->twig->render('supplierNew.html.twig' , [
                    'sup' => $sup,
                    'errors' => $errors
                ]);
            }
        } else {
            return $this->twig->render('supplierNew.html.twig');
        }
    }
}