<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 20-09-18
 * Time: 11:40
 */

namespace PhpOO\controllers;


use PhpOO\Models\Product;
use PhpOO\Models\ProductSupplier;
use PhpOO\Repository\ProductRepository;
use PhpOO\Repository\ProductSupplierRepository;
use PhpOO\Repository\SupplierRepository;

class ProductSupplierController
{

    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new ProductSupplierRepository();
        $proSups = $repo->getAll();
        return $this->twig->render('productSupplierList.html.twig', [
            'proSups' => $proSups
        ]);
    }

    public function add($post = null)
    {
        if (isset($post['productID'])) {
            var_dump($post);
            $prodSup = new ProductSupplier();
            $prodSup->setProductID($post['productID']);
            $prodSup->setSupplierID($post['supplierID']);
            $prodSup->setPrice($post['price']);
            $repo = new ProductSupplierRepository();
            $repo->insert($prodSup);
            if ($repo) {
               header('location: localhost/phpoo/web/app.php?controller=productSupplier&method=index');
            }
        } else {

            $repo = new ProductRepository();
            $prodLists = $repo->getAll();

            $repo = new SupplierRepository();
            $suppLists = $repo->getAll();

            return $this->twig->render('productSupplierNew.html.twig', [
                'prodLists' => $prodLists,
                'suppLists' => $suppLists
            ]);
        }
    }
}