<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 13-09-18
 * Time: 10:02
 */

namespace Test;


class Epargne extends BaseCompte
{
    /**
     * @var \DateTime
     */
    private $dateCreation;

    public function __construct($numero)
    {
        parent::__construct($numero);
    }

    /**
     * @param int $montant
     */
    public function retirer($montant)
    {
        parent::retirer($montant);
        if ($montant > $this->solde) {
            throw new \InvalidArgumentException();
        }

        $this->solde -= $montant;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }
}