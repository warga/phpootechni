<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 13-09-18
 * Time: 10:02
 */

namespace Test;


class Courant extends BaseCompte
{
    private $limitDeCredit;

    /**
     * @return float
     */
    public function getLimitDeCredit()
    {
        return $this->limitDeCredit;
    }

    /**
     * @param float $limitDeCredit
     * @return Courant
     */
    public function setLimitDeCredit($limitDeCredit)
    {
        if ($limitDeCredit > 10000) {
            throw new \InvalidArgumentException();
        }

        $this->limitDeCredit = $limitDeCredit;
        return $this;
    }

    public function retirer($montant)
    {
        parent::retirer($montant);
        if ($montant > $this->getSolde() + $this->limitDeCredit) {
            throw new \InvalidArgumentException();
        }
        $this->solde -= $montant;
    }
}