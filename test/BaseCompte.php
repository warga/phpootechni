<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 13-09-18
 * Time: 10:04
 */

namespace Test;


abstract class BaseCompte
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $number;

    /**
     * @var float
     */
    protected $solde;

    public function __construct($toto)
    {
        $this->solde = $toto;
    }

    /**
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $client
     * @return BaseCompte
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return BaseCompte
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * @param integer $montant
     */
    public function retirer($montant)
    {
        if ($montant <= 0) {
            throw new \InvalidArgumentException();
        }
    }

    /**
     * @param integer $montant
     */
    public function ajouter($montant)
    {
        if ($montant <= 0) {
            throw new \InvalidArgumentException();
        }

        $this->solde = $this->getSolde() + $montant;
    }
}