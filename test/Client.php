<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 13-09-18
 * Time: 09:33
 */

namespace Test;


class Client
{
    private $name;
    private $firstName;
    private $accountNumber;

    public function __construct($accountN)
    {
        if (strlen($accountN) != 20) {
            throw new \InvalidArgumentException();
        }
        $this->accountNumber = $accountN;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Client
     * @throws \Exception
     */
    public function setName($name)
    {
        if(!isset($name) || $name == NULL) {
            throw new \Exception();
        }
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Client
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return integer
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }
}