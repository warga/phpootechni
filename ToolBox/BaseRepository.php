<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 12-09-18
 * Time: 13:11
 */

namespace ToolBox;

use PDO;

abstract class BaseRepository
{
    protected $pdo;
    public function __construct()
    {
        $this->pdo = new PDO(
            "mysql:host=localhost;dbname=phpoo;charset=utf8",
            "root",
            null
        );
    }
    protected abstract function getTableName();
    protected abstract function getPKName();
    protected abstract function getEntityName();
    protected abstract function getBindings();


    public function getByID($item)
    {
        $query = "SELECT * FROM ";
        $query .= $this->getTableName();
        $query .= " WHERE ";
        foreach ($this->getPKName() as $k => $v){
            $query .= $k;
            $query .= " = :";
            $query .= $k;
            $query .= ' ,';
            $params[$k] = $item->{"get" . ucwords($v)}();
        }
        $query = substr($query, 0, -1);

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchObject($this->getEntityName());
    }

    public function getAll()
    {
        $bindings = $this->getBindings();
        $query = "SELECT ";
        $query .= implode(",", array_keys($bindings));
        $query .= ", ";
        $query .= implode(",", array_keys($this->getPKName()));
        $query .= " FROM ";
        $query .= $this->getTableName();


        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, $this->getEntityName());
    }

    public function insert($item)
    {
        $bindings = $this->getBindings();
        $query = "INSERT INTO ";
        $query .= $this->getTableName();
        $query .= "(";
        $query .= implode(",", array_keys($bindings));
        $query .= ") VALUES (:";
        $query .= implode(",:", array_keys($bindings));
        $query .= ")";
        $stmt = $this->pdo->prepare($query);
        $tab = [];
        foreach ($bindings as $key => $value){
            $tab[":" . $key] = $item->{$value}();
        }

        $result = $stmt->execute($tab);
        if ($result && count($this->getPKName()) === 1){
            $item->{"get" . ucwords(array_keys($this->getPKName())[0])}($this->pdo->lastInsertId());
        }
        return $result;
    }

    public function Update($item){
        $bindings = $this->getBindings();
        $query = "UPDATE ";
        $query .= $this->getTableName();
        $query .= " set ";
        foreach ($bindings as $key => $value){
            $query .= $key;
            $query .= "=:";
            $query .= $key;
            $query .= ",";
        }
        $query = substr($query, 0, -1);
        $query .= " WHERE ";
        foreach ($this->getPKName() as $k => $v) {
            $query .= $k;
            $query .= " = :";
            $query .= $k;
            $query .= " ,";
            $tab[":" . $k] = $item->{$v}();
        }
        $query = substr($query, 0, -1);

        $stmt = $this->pdo->prepare($query);
        $params = [];
        foreach ($bindings as $key => $value){
            $params[":" . $key] = $item->{$value}();
        }

        $result = $stmt->execute($params);
        return $result;
    }


    public function delete($item)
    {
        $query = "DELETE FROM ";
        $query .= $this->getTableName();
        $query .= " WHERE ";
        $params = [];
        foreach ($this->getPKName() as $k => $v) {
            $query .= $k;
            $query .= " = :";
            $query .= $k;
            $query .= " ,";
            $params = [
                $k => $item->{"get" . ucwords($v)}()
            ];
        }
        $query = substr($query, 0, -1);

        $stmt = $this->pdo->prepare($query);

        $result = $stmt->execute($params);
        return $result;
    }
}