<?php
/**
 * Created by PhpStorm.
 * User: studentsc21
 * Date: 19-09-18
 * Time: 15:28
 */

namespace ToolBox;


class Router
{
    public function handleRoute($twig, $get, $post)
    {
        if (isset($get["controller"])) {
            $controllerName = $get["controller"];
        } else {
            $controllerName = 'product';
            $get["method"] = 'index';
        }

            $className = "\\PhpOO\\Controllers\\"
                . ucfirst($controllerName)
                . "Controller";

            $controller = new $className($twig);

        if (isset($get["method"])) {
            if (isset($get["id"])) {
                if (empty($post)) {
                    return $controller->{$get["method"]}($get["id"]);
                } else {
                    return $controller->{$get["method"]}($get["id"], $post);
                }
            } else {
                if (empty($post)) {
                    return $controller->{$get["method"]}();
                } else {
                    return $controller->{$get["method"]}($post);
                }
            }
        }
    }
}